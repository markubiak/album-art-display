#! /bin/bash

# lightweight wrapper to launch QT5 display from LXDE autostart script
cd /home/pi/album-art-display
python3 ./fullscreen_display.py
