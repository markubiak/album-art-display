""" Displays album art """

import sys
import queue

# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QPixmap, QPalette, QFont
from PyQt5.QtCore import Qt, QObject, QThread, QTimer, pyqtSignal
from PIL.ImageQt import ImageQt

class Communicate(QObject):

    """ Naive wrapper class for a pyqtSignal """
    sig = pyqtSignal()

class AlbumDisplay(QWidget):

    """ Displays album art using PyQT5 """
    def __init__(self, flask_app, parent=None):
        super(AlbumDisplay, self).__init__(parent)
        self.img_label = QLabel(self)

        self.flask_app = flask_app
        self.comm = Communicate()
        self.comm.sig.connect(self.update)

        self.timer = QTimer()
        self.timer.setInterval(250)
        self.timer.timeout.connect(self.update)

        self.setAutoFillBackground(True)
        self.setPalette(QPalette(Qt.black))

        self.img_label.setAlignment(Qt.AlignLeft)

        main_view = QHBoxLayout()
        main_view.addWidget(self.img_label)
        main_view.addStretch(1)
        main_view.setContentsMargins(0, 0, 0, 0)

        self.setLayout(main_view)

        self.showFullScreen()

        self.timer.start()

    # pylint: disable=unused-argument,invalid-name
    def resizeEvent(self, event):
        """ called on window resize """
        self.update()

    def update(self):
        """ shows the new album art and metadata """
        try:
            song_info = self.flask_app.q.get_nowait()
            print("Updating displayed image...")
            qt_img = ImageQt(song_info.img)
            pixmap = QPixmap.fromImage(qt_img)
            pixmap = pixmap.scaled(
                self.width(), self.height(),
                aspectRatioMode=Qt.KeepAspectRatio)
            self.img_label.setPixmap(pixmap)
            min_dim = min(self.width(), self.height())
            self.img_label.resize(min_dim, min_dim)
        except queue.Empty:
            pass

class FlaskThread(QThread):
    """ QT thread that wraps Flask """

    def __init__(self, application):
        QThread.__init__(self)
        self.application = application

    def __del__(self):
        self.wait()

    def run(self):
        """ Starts Flask server """
        self.application.run()

# pylint: disable=invalid-name
if __name__ == '__main__':
    from routes import app
    qtapp = QApplication(sys.argv)
    ex = AlbumDisplay(app)
    app.qt_comm = ex.comm
    webapp = FlaskThread(app)
    webapp.start()
    qtapp.aboutToQuit.connect(webapp.terminate)
    sys.exit(qtapp.exec_())
