#! /usr/bin/env python3

import os
import pdb

import numpy as np

from PIL import Image, ImageOps, ImageFont, ImageDraw


font_medium = ImageFont.truetype("Roboto-Medium.ttf", 72)
font_light = ImageFont.truetype("Roboto-Light.ttf", 60)


# Find the primary color of the image
def _find_primary_color(im):

    # constants
    decim = 16
    greyscale_factor = 0.7

    # Get the color histogram
    n_px = im.size[0] * im.size[1]
    ima = np.array(im).reshape((n_px, 3))
    hist, _ = np.histogramdd(ima, bins=decim)

    # De-emphasize greyscale colors
    for i in range(decim):
        hist[i][i][i] *= greyscale_factor

    # Find our primary color after decimation
    color_decim = np.unravel_index(hist.argmax(), hist.shape)
    decim_range = 256 // decim
    color_decim = (np.array(color_decim) * decim_range) + round(decim_range / 2)


    # Extract the most common color in our decimated range
    color = color_decim
    color = (color[0], color[1], color[2]) # to tuple
    return color

def _text_wrap(text, font, max_width):

    lines = []
    # ImageDraw on ignored image
    draw = ImageDraw.Draw(Image.new('RGB', (1, 1)))

    # If the text width is smaller than the image width, then no need to split
    # just add it to the line list and return
    if draw.multiline_textsize(text, font=font)[0] <= max_width:
        lines.append(text)
    else:
        # split the line by spaces to get words
        words = text.split(' ')
        i = 0
        # append every word to a line while its width is shorter than the image width
        while i < len(words):
            line = ''
            while i < len(words) and draw.multiline_textsize(line + words[i], font=font)[0] <= max_width:
                line = line + words[i] + " "
                i += 1
            if not line:
                line = words[i]
                i += 1
            lines.append(line)
    return '\n'.join(lines)

# Change a square input image into an arbitrary-dimension output image
# with a gradient into the background color to fill in the longer
# side of the target image
def fade_color_background(album_art, target_dims):

    # Constants
    fade_percentage = 40
    sampling_percentage = 20
    bottom_padding_percentage = 10

    # Resize our album art to match target dims
    album_edge_px = min(target_dims)
    album_art = ImageOps.fit(album_art,
                             (album_edge_px, album_edge_px),
                             method=Image.ANTIALIAS)

    # Generate our alpha layer for the album art
    alpha = np.ones((album_edge_px, album_edge_px))
    n_rows = round((fade_percentage / 100) * album_edge_px)
    # extra opacity at bottom smooths out transition
    bottom_padding = bottom_padding_percentage / 100
    for row in range(n_rows):
        a = ((row / n_rows) * (1.0 + bottom_padding)) - bottom_padding
        dither = np.random.normal(0, 0.02, alpha.shape[0])
        alpha[album_edge_px-1-row, :] = np.clip(a + dither, 0.0, 1.0)

    alpha *= 255
    alpha = alpha.astype(np.uint8)

    # Determine the direction of the gradient and
    # sampling region for the background color
    sampling_region = round((sampling_percentage / 100) * album_edge_px)
    if target_dims[0] > target_dims[1]: # Horizontal
        alpha = alpha.T
        bg_color = _find_primary_color(
                ImageOps.crop(album_art, (sampling_region, 0, 0, 0)))
    else:
        bg_color = _find_primary_color(
                ImageOps.crop(album_art, (0, sampling_region, 0, 0)))

    # Generate a new image of the passed in color
    # and paste our original image with alpha mask
    alpha = Image.fromarray(alpha)
    im = Image.new('RGB', target_dims, bg_color)
    im.paste(album_art, (0, 0), mask=alpha)
    return im, bg_color


def make_description_horizontal(target_dims, song, artist, album, color):

    """ Generates an image of target_dims with text showing the song, artist,
    and album in a given color. Right justified, vertically centered, wrapped
    assuming left side of image is a square album art.
    """

    # Setup the fonts and Draw object
    text_img = Image.new('RGBA', target_dims, (0,0,0,0))
    draw = ImageDraw.Draw(text_img)

    # Get the sizes
    max_width = (target_dims[0] - target_dims[1] - 80)
    song = _text_wrap(song, font_medium, max_width)
    artist = _text_wrap(artist, font_light, max_width)
    album = _text_wrap(album, font_light, max_width)
    song_width, song_height = draw.multiline_textsize(song, font=font_medium)
    artist_width, artist_height = draw.multiline_textsize(artist, font=font_light)
    album_width, album_height = draw.multiline_textsize(album, font=font_light)

    # Calculate our starting position, assumes 4:3 vertical
    right_x = text_img.width - 40 # padding
    center_y = round(text_img.height / 2)
    line1_padding = 40
    line2_padding = 30
    text_height = song_height + line1_padding + artist_height + \
                  line2_padding + album_height
    top_y = center_y - round(text_height / 2)

    # Draw the text
    draw.multiline_text((right_x - song_width, top_y), \
            song, font=font_medium, fill=color, align="right")
    draw.multiline_text((right_x - artist_width, \
               top_y + song_height + line1_padding), \
              artist, font=font_light, fill=color, align="right")
    draw.multiline_text((right_x - album_width, \
               top_y + song_height + line1_padding + \
               artist_height + line2_padding), \
              album, font=font_light, fill=color, align="right")

    # Return RGBA image
    return text_img
