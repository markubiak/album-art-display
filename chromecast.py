#! /usr/bin/env python3

""" Entry point for album art display """

import sys
from time import sleep

import pychromecast
import requests

# pylint: disable=too-few-public-methods
class StatusMediaListener:

    """ Listen for Chromecast meida player broadcasts """

    def __init__(self, name, cast):
        self.name = name
        self.cast = cast

    # pylint: disable=no-self-use
    def new_media_status(self, status):

        """ Called asynchronously when any new status is found """

        # print(status)
        images = status.images
        # filter out only statuses with images
        if not images:
            return
        # find largest image
        max_idx = -1
        max_size = -1
        for i, image in enumerate(images):
            size = image.width + image.height  # naive method of dimension resolution
            if size > max_size:
                max_idx = i
                max_size = size
        largest_image = images[max_idx]
        # download and display it
        print("Showing album art for %s - %s" % (status.title, status.artist))
        try:
            requests.put('http://localhost:5000/show', json={
                "url": largest_image.url,
                "song": status.title,
                "album": status.album_name,
                "artist": status.artist
            })
        except requests.exceptions.Timeout:
            print("Album update request timed out! Ignoring...")
        except requests.exceptions.ConnectionError:
            print("Could not connect to album display server! Ignoring...")

def start_listening(name):

    """ Adds a statusMediaListener to the specified chromecast """

    chromecasts, _ = pychromecast.get_listed_chromecasts(friendly_names=[name])
    if len(chromecasts) < 1:
        print("No chromecasts found! Exiting...")
        sys.exit(1)
    chromecast = chromecasts[0]
    print("Listening to %s" % (chromecast.name))
    chromecast.start()
    listener_media = StatusMediaListener(chromecast.name, chromecast)
    chromecast.media_controller.register_status_listener(listener_media)

    print("Waiting for events, press [Ctrl+C] to quit...\n")

    # Hacky thread parking
    while True:
        sleep(3)


if __name__ == '__main__':

    # hardcoded config path, nice.
    with open('/etc/album-display/chromecast', 'r') as f:
        name = f.read().strip()
    start_listening(name)
