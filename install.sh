#! /bin/bash

# TODO
# /boot/config.txt 1080p
# feh background
# wait for network on boot

# Stop the script if any of these fail
set -e

# User prompts
read -r -p "Enter the name of the Chromecast, as it appears on your network (CASE SENSITIVE): " CHROMECAST_NAME
read -r -p "I got $CHROMECAST_NAME, is this correct? [y/N] " RESP
if [[ ! "$RESP" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    echo "User did not confirm Chromecast name, exiting..."
    exit 1
fi
sudo mkdir -p /etc/album-display
echo $CHROMECAST_NAME | sudo tee /etc/album-display/chromecast
echo "If you ever need to update this, edit /etc/album-display/chromecast"
echo "Installing, this may take awhile..."
echo
echo

# Update repos and install required packages
sudo apt -y update
sudo apt -y upgrade
sudo apt -y install python3-numpy python3-requests python3-pillow python3-flask
pip3 install --user pychromecast
sudo apt -y install qt5-default pyqt5-dev pyqt5-dev-tools
sudo apt -y install libjpeg-dev zlib1g-dev libfreetype6-dev liblcms1-dev libopenjp2-7 libtiff5

# Install configuration files and startup scripts
sudo mkdir -p /etc/systemd/user
sudo cp chromecast-server.service /etc/systemd/user/chromecast-server.service
systemctl --user daemon-reload
systemctl --user enable chromecast-server.service
mkdir -p /home/pi/.config/lxsession/LXDE-pi
cp autostart /home/pi/.config/lxsession/LXDE-pi/autostart
sudo sed -i 's/#xserver-command=X/xserver-command=X -nocursor -s 0 -dpms/g' /etc/lightdm/lightdm.conf

# Feedback
echo
echo
echo "Installation complete! Please reboot your Pi."
