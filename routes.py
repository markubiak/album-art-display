""" Receives PUT requests to display new images """

# pylint: disable=all
# import multiprocessing
import ctypes
import os
import signal
import sys
import subprocess
import threading
import time
import queue

import numpy as np
from flask import Flask, request
from PIL import Image, ImageOps
import requests

import imgops

app = Flask(__name__)
app.q = queue.Queue()

art_thread = None

song_info = None


# https://gist.github.com/liuw/2407154
def ctype_async_raise(thread_obj, exception):
    found = False
    target_tid = 0
    for tid, tobj in threading._active.items():
        if tobj is thread_obj:
            found = True
            target_tid = tid
            break

    if not found:
        raise ValueError("Invalid thread object")

    # Change to c_long for 64 bit arch
    ret = ctypes.pythonapi.PyThreadState_SetAsyncExc(
            ctypes.c_int(target_tid),
            ctypes.py_object(exception))
    # ref: http://docs.python.org/c-api/init.html#PyThreadState_SetAsyncExc
    if ret == 0:
        raise ValueError("Invalid thread ID")
    elif ret > 1:
        # Huh? Why would we notify more than one threads?
        # Because we punch a hole into C level interpreter.
        # So it is better to clean up the mess.
        ctypes.pythonapi.PyThreadState_SetAsyncExc(target_tid, NULL)
        raise SystemError("PyThreadState_SetAsyncExc failed")


class SongInfo:
    """ Class containing info about a currently playing song """

    def __init__(self, data):

        # "url" required, others optional
        self.url = data["url"]
        self.song = data.get("song")
        self.album = data.get("album")
        self.artist = data.get("artist")

        # Change from None to empty strings
        self.song = "" if self.song is None else self.song
        self.album = "" if self.album is None else self.album
        self.artist = "" if self.artist is None else self.artist

    def __eq__(self, other):
        """ Overloaded for easy update checking"""
        return self.url == other.url and \
                self.song == other.song and \
                self.album == other.album and \
                self.artist == other.artist

    def update_art_async(self):
        """ Downloads url, generating background"""

        print("Downloading and generating %s" % self.url)

        # Abort anything in progress
        global art_thread
        if art_thread is not None and art_thread.is_alive():
            print("Killing existing run...")
            ctype_async_raise(art_thread, signal.SIGINT)

        # Launch new art gen
        art_thread = threading.Thread(
                target=self._thread_download_and_generate)
        art_thread.start()

    def _thread_download_and_generate(self):
        """ Thread worker, downloads and generates composite image.
        Delegated to thread so it can be cancelled if skip is repeatedly
        pressed by using the hacky ctype_async_raise()
        """

        try:
            print("Downloading image...")
            resp = requests.get(self.url, stream=True)
            im = Image.open(resp.raw).convert('RGB')
            print("Generating display image...")
            im, bg_color = imgops.fade_color_background(im, (1920, 1080))
            text_color = 'white' if (np.sum(bg_color) / 768.0) < 0.5 else 'black'
            text = imgops.make_description_horizontal((1920, 1080), self.song, self.artist, self.album, text_color)
            self.img = Image.alpha_composite(im.convert('RGBA'), text)
            app.q.put(self)
            print("Display image generated.")
        except Exception:
            print("Closing running worker thread")


@app.route("/show", methods=['PUT'])
def show():

    """ Receives a URL to download and makes it the wallpaper """

    global song_info
    if request.method == 'PUT':
        data = request.json
        new_song_info = SongInfo(data)

        if song_info is None or \
            new_song_info != song_info:

            song_info = new_song_info
            song_info.update_art_async()

        return "OK"
    return super()

if __name__ == "__main__":
    app.run(debug=False)
